/*
Inside of routes.js, create an exportable function that will handle different endpoints
for our server:

For the "/" endpoints, display a message saying "This is the homepage"
For the "/login" endpoints, display a message saying "This is the login page"
For the "/about" endpoints, display a message saying "This is the about page"
For the other endpoints, display a message saying "Page not found" with a 404 status page
Import that function into index.js and add it to the createServer() method
*/

module.exports.aaa = (request, response) =>{
	if(request.url === '/'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('This is the homepage')

	}else if(request.url === '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('This is the login page')

	}else if(request.url === '/about'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('This is the about page')

	}else{
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Page not available')
	}
}









